(use-package org
  :defer t
  :bind (("\C-cl" . 'org-store-link)
	 ("\C-ca" . 'org-agenda)
	 ("\C-cc" . 'org-capture)
	 ("\C-cb" . 'org-iswitchb)
	 ("\C-cs" . 'org-save-all-org-buffers))
  :config
  (progn
    (if location-is-work-p (progn
			     (setq org-directory "~/Dropbox/org")
			     (setq org-default-notes-file "~/Dropbox/org/refile.org")
			     (setq org-agenda-files (quote ("~/dropbox/org"
							    "~/dropbox/uni/2019-fall/ECE4273/ECE4273.org"
							    "~/dropbox/uni/2019-fall/ECE5523/ECE5523.org"
							    "~/dropbox/uni/2019-fall/ECE5663/ECE5663.org"
							    "~/dropbox/uni/2019-fall/ECE5973/ECE5973.org")))))
    (if location-is-home-p (progn
			     (setq org-directory "~/Dropbox/org")
			     (setq org-default-notes-file "~/Dropbox/org/refile.org")
			     (setq org-agenda-files (quote ("~/dropbox/org"
                                          "~/dropbox/uni/2019-fall/ECE4273/ECE4273.org"
                                          "~/dropbox/uni/2019-fall/ECE5523/ECE5523.org"
                                          "~/dropbox/uni/2019-fall/ECE5663/ECE5663.org"
                                          "~/dropbox/uni/2019-fall/ECE5973/ECE5973.org")))))

    (if location-is-laptop-p (progn
			     (setq org-directory "~/Dropbox/org")
			     (setq org-default-notes-file "~/Dropbox/org/refile.org")
			     (setq org-agenda-files (quote ("~/dropbox/org"
                                          "~/dropbox/uni/2019-fall/ECE4273/ECE4273.org"
                                          "~/dropbox/uni/2019-fall/ECE5523/ECE5523.org"
                                          "~/dropbox/uni/2019-fall/ECE5663/ECE5663.org"
                                          "~/dropbox/uni/2019-fall/ECE5973/ECE5973.org")))))
    (if location-is-desktop-p (progn
			     (setq org-directory "F:/Dropbox/org")
			     (setq org-default-notes-file "F:/Dropbox/org/refile.org")
			     (setq org-agenda-files (quote ("F:/Dropbox/org"
                                          "F:/Dropbox/uni/2019-fall/ECE4273/ECE4273.org"
                                          "F:/Dropbox/uni/2019-fall/ECE5523/ECE5523.org"
                                          "F:/Dropbox/uni/2019-fall/ECE5663/ECE5663.org"
                                          "F:/Dropbox/uni/2019-fall/ECE5973/ECE5973.org")))))

    (add-hook 'org-capture-mode-hook 'evil-insert-state)

    ;; Behavior and appearance
    (setq org-startup-indented t)
    (setq org-agenda-start-on-weekday nil)

    ;; Enabling optional modules
    (add-to-list 'org-modules 'org-habit)

    ;; Habits
    ; (setq org-habit-graph-column 80)

    ;; Set timestamp format
    (setq calendar-date-style 'iso)

    ;; Tasks and states
    (setq org-todo-keywords
	  (quote ((sequence "TODO(t)" "NEXT(n)" "|" "DONE(d)")
		  (sequence "WAITING(w)" "DEFERRED(f)" "|" "CANCELLED(c)" "MAYBE(m)" "MEETING(M)"))))

    (setq org-todo-keyword-faces
	  (quote (("TODO" :foreground "red" :weight bold)
		  ("NEXT" :foreground "blue" :weight bold)
		  ("DONE" :foreground "forest green" :weight bold)
		  ("WAITING" :foreground "orange" :weight bold)
		  ;;("HOLD" :foreground "magenta" :weight bold)
		  ("CANCELLED" :foreground "forest green" :weight bold)
		  ("MEETING" :foreground "forest green" :weight bold)
		  ("DEFERRED" :foreground "forest green" :weight bold)
		  ("MAYBE" :foreground "forest green" :weight bold)
		  ;;("PHONE" :foreground "forest green" :weight bold)
		  )))

    (setq org-todo-state-tags-triggers
	  (quote (("CANCELLED" ("CANCELLED" . t))
		  ("WAITING" ("WAITING" . t))
		  ("HOLD" ("WAITING") ("HOLD" . t))
		  (done ("WAITING") ("HOLD"))
		  ("TODO" ("WAITING") ("CANCELLED") ("HOLD"))
		  ("NEXT" ("WAITING") ("CANCELLED") ("HOLD"))
		  ("DONE" ("WAITING") ("CANCELLED") ("HOLD")))))

    (setq org-enforce-todo-dependencies t)

    ;; Setup context and domain tags
    (setq org-tag-alist (quote (("Work" . 119)
				("Personal" . 112)
				("College" . 99)
				("@School" . 83)
				("@Office" . 79)
				("@Home" . 72)
				("@Errands" . 69)
				("@Computer" . 67)
				("@Telephone" . 84)
				("@Anywhere" . 65)
				("Reading" . ?r)
				("CANCELLED" . ?x)
				("WAITING" . ?y)
				("HOLD" . ?z))))

					; For tag searches ignore tasks with scheduled and deadline dates
    (setq org-agenda-tags-todo-honor-ignore-options t)

    (setq org-capture-templates
	  (quote (("t" "todo" entry (file "refile.org")
		   "* TODO %?%^g\n")
		  ("r" "respond" entry (file "refile.org")
		   "* TODO Respond to %^{NAME} on %^{SUBJECT}%^g\nSCHEDULED: %^T")
		  ("a" "appointment" entry (file "refile.org")
		   "* APPOINTMENT with %?%^g\nSCHEDULED: %^T"))))

    ;; Setup refiling
    ;; Targets are the present file, and all agenda files
    (setq org-refile-targets (quote ((nil :maxlevel . 9)
				     (org-agenda-files :maxlevel . 9))))

    (setq org-refile-use-outline-path 'file)
    (setq org-outline-path-complete-in-steps nil)
    (setq org-completion-use-ido nil)
    (setq org-refile-allow-creating-parent-nodes (quote confirm))

    ;; Functions for handling projects
    ;; Projects are defined as any task with subtasks
    ;; Begin by disabling org's default stuck projects function
    (setq org-stuck-projects (quote ("" nil nil "")))

    ;; From doc.norang.ca/org-mode.html#Projects
    (defun my/find-project-task ()
      "Move point to the parent (project) task if any"
      (save-restriction
	(widen)
	(let ((parent-task (save-excursion (org-back-to-heading 'invisible-ok) (point))))
	  (while (org-up-heading-safe)
	    (when (member (nth 2 (org-heading-components)) org-todo-keywords-1)
	      (setq parent-task (point))))
	  (goto-char parent-task)
	  parent-task)))

    (defun my/is-project-p ()
      "Any task with a todo keyword subtask"
      (save-restriction
	(widen)
	(let ((has-subtask)
	      (subtree-end (save-excursion (org-end-of-subtree t)))
	      (is-a-task (member (nth 2 (org-heading-components)) org-todo-keywords-1)))
	  (save-excursion
	    (forward-line 1)
	    (while (and (not has-subtask)
			(< (point) subtree-end)
			(re-search-forward "^\*+ " subtree-end t))
	      (when (member (org-get-todo-state) org-todo-keywords-1)
		(setq has-subtask t))))
	  (and is-a-task has-subtask))))

    (defun my/is-project-subtree-p ()
      "Any task with a todo keyword that is in a project subtree.
Callers of this function already widen the buffer view."
      (let ((task (save-excursion (org-back-to-heading 'invisible-ok)
				  (point))))
	(save-excursion
	  (my/find-project-task)
	  (if (equal (point) task)
	      nil
	    t))))

    (defun my/is-task-p ()
      "Any task with a todo keyword and no subtask"
      (save-restriction
	(widen)
	(let ((has-subtask)
	      (subtree-end (save-excursion (org-end-of-subtree t)))
	      (is-a-task (member (nth 2 (org-heading-components)) org-todo-keywords-1)))
	  (save-excursion
	    (forward-line 1)
	    (while (and (not has-subtask)
			(< (point) subtree-end)
			(re-search-forward "^\*+ " subtree-end t))
	      (when (member (org-get-todo-state) org-todo-keywords-1)
		(setq has-subtask t))))
	  (and is-a-task (not has-subtask)))))

    (defun my/is-subproject-p ()
      "Any task which is a subtask of another project"
      (let ((is-subproject)
	    (is-a-task (member (nth 2 (org-heading-components)) org-todo-keywords-1)))
	(save-excursion
	  (while (and (not is-subproject) (org-up-heading-safe))
	    (when (member (nth 2 (org-heading-components)) org-todo-keywords-1)
	      (setq is-subproject t))))
	(and is-a-task is-subproject)))

    (defun my/list-sublevels-for-projects-indented ()
      "Set org-tags-match-list-sublevels so when restricted to a subtree we list all subtasks.
  This is normally used by skipping functions where this variable is already local to the agenda."
      (if (marker-buffer org-agenda-restrict-begin)
	  (setq org-tags-match-list-sublevels 'indented)
	(setq org-tags-match-list-sublevels nil))
      nil)

    (defun my/list-sublevels-for-projects ()
      "Set org-tags-match-list-sublevels so when restricted to a subtree we list all subtasks.
  This is normally used by skipping functions where this variable is already local to the agenda."
      (if (marker-buffer org-agenda-restrict-begin)
	  (setq org-tags-match-list-sublevels t)
	(setq org-tags-match-list-sublevels nil))
      nil)

    (defvar my/hide-scheduled-and-waiting-next-tasks t)

    (defun my/toggle-next-task-display ()
      (interactive)
      (setq my/hide-scheduled-and-waiting-next-tasks (not my/hide-scheduled-and-waiting-next-tasks))
      (when  (equal major-mode 'org-agenda-mode)
	(org-agenda-redo))
      (message "%s WAITING and SCHEDULED NEXT Tasks" (if my/hide-scheduled-and-waiting-next-tasks "Hide" "Show")))

    (defun my/skip-stuck-projects ()
      "Skip trees that are not stuck projects"
      (save-restriction
	(widen)
	(let ((next-headline (save-excursion (or (outline-next-heading) (point-max)))))
	  (if (my/is-project-p)
	      (let* ((subtree-end (save-excursion (org-end-of-subtree t)))
		     (has-next ))
		(save-excursion
		  (forward-line 1)
		  (while (and (not has-next) (< (point) subtree-end) (re-search-forward "^\\*+ NEXT " subtree-end t))
		    (unless (member "WAITING" (org-get-tags-at))
		      (setq has-next t))))
		(if has-next
		    nil
		  next-headline)) ; a stuck project, has subtasks but no next task
	    nil))))

    (defun my/skip-non-stuck-projects ()
      "Skip trees that are not stuck projects"
      ;; (bh/list-sublevels-for-projects-indented)
      (save-restriction
	(widen)
	(let ((next-headline (save-excursion (or (outline-next-heading) (point-max)))))
	  (if (my/is-project-p)
	      (let* ((subtree-end (save-excursion (org-end-of-subtree t)))
		     (has-next ))
		(save-excursion
		  (forward-line 1)
		  (while (and (not has-next) (< (point) subtree-end) (re-search-forward "^\\*+ NEXT " subtree-end t))
		    (unless (member "WAITING" (org-get-tags-at))
		      (setq has-next t))))
		(if has-next
		    next-headline
		  nil)) ; a stuck project, has subtasks but no next task
	    next-headline))))

    (defun my/skip-non-projects ()
      "Skip trees that are not projects"
      ;; (my/list-sublevels-for-projects-indented)
      (if (save-excursion (my/skip-non-stuck-projects))
	  (save-restriction
	    (widen)
	    (let ((subtree-end (save-excursion (org-end-of-subtree t))))
	      (cond
	       ((my/is-project-p)
		nil)
	       ((and (my/is-project-subtree-p) (not (my/is-task-p)))
		nil)
	       (t
		subtree-end))))
	(save-excursion (org-end-of-subtree t))))

    (defun my/skip-non-tasks ()
      "Show non-project tasks.
Skip project and sub-project tasks, habits, and project related tasks."
      (save-restriction
	(widen)
	(let ((next-headline (save-excursion (or (outline-next-heading) (point-max)))))
	  (cond
	   ((my/is-task-p)
	    nil)
	   (t
	    next-headline)))))

    (defun my/skip-project-trees-and-habits ()
      "Skip trees that are projects"
      (save-restriction
	(widen)
	(let ((subtree-end (save-excursion (org-end-of-subtree t))))
	  (cond
	   ((my/is-project-p)
	    subtree-end)
	   ((org-is-habit-p)
	    subtree-end)
	   (t
	    nil)))))

    (defun my/skip-projects-and-habits-and-single-tasks ()
      "Skip trees that are projects, tasks that are habits, single non-project tasks"
      (save-restriction
	(widen)
	(let ((next-headline (save-excursion (or (outline-next-heading) (point-max)))))
	  (cond
	   ((org-is-habit-p)
	    next-headline)
	   ((and my/hide-scheduled-and-waiting-next-tasks
		 (member "WAITING" (org-get-tags-at)))
	    next-headline)
	   ((my/is-project-p)
	    next-headline)
	   ((and (my/is-task-p) (not (my/is-project-subtree-p)))
	    next-headline)
	   (t
	    nil)))))

    (defun my/skip-project-tasks-maybe ()
      "Show tasks related to the current restriction.
When restricted to a project, skip project and sub project tasks, habits, NEXT tasks, and loose tasks.
When not restricted, skip project and sub-project tasks, habits, and project related tasks."
      (save-restriction
	(widen)
	(let* ((subtree-end (save-excursion (org-end-of-subtree t)))
	       (next-headline (save-excursion (or (outline-next-heading) (point-max))))
	       (limit-to-project (marker-buffer org-agenda-restrict-begin)))
	  (cond
	   ((my/is-project-p)
	    next-headline)
	   ((org-is-habit-p)
	    subtree-end)
	   ((and (not limit-to-project)
		 (my/is-project-subtree-p))
	    subtree-end)
	   ((and limit-to-project
		 (my/is-project-subtree-p)
		 (member (org-get-todo-state) (list "NEXT")))
	    subtree-end)
	   (t
	    nil)))))

    (defun my/skip-project-tasks ()
      "Show non-project tasks.
Skip project and sub-project tasks, habits, and project related tasks."
      (save-restriction
	(widen)
	(let* ((subtree-end (save-excursion (org-end-of-subtree t))))
	  (cond
	   ((my/is-project-p)
	    subtree-end)
	   ((org-is-habit-p)
	    subtree-end)
	   ((my/is-project-subtree-p)
	    subtree-end)
	   (t
	    nil)))))

    (defun my/skip-non-project-tasks ()
      "Show project tasks.
Skip project and sub-project tasks, habits, and loose non-project tasks."
      (save-restriction
	(widen)
	(let* ((subtree-end (save-excursion (org-end-of-subtree t)))
	       (next-headline (save-excursion (or (outline-next-heading) (point-max)))))
	  (cond
	   ((my/is-project-p)
	    next-headline)
	   ((org-is-habit-p)
	    subtree-end)
	   ((and (my/is-project-subtree-p)
		 (member (org-get-todo-state) (list "NEXT")))
	    subtree-end)
	   ((not (my/is-project-subtree-p))
	    subtree-end)
	   (t
	    nil)))))

    (defun my/skip-projects-and-habits ()
      "Skip trees that are projects and tasks that are habits"
      (save-restriction
	(widen)
	(let ((subtree-end (save-excursion (org-end-of-subtree t))))
	  (cond
	   ((my/is-project-p)
	    subtree-end)
	   ((org-is-habit-p)
	    subtree-end)
	   (t
	    nil)))))

    (defun my/skip-non-subprojects ()
      "Skip trees that are not projects"
      (let ((next-headline (save-excursion (outline-next-heading))))
	(if (my/is-subproject-p)
	    nil
	  next-headline)))

    (defun my/skip-non-archivable-tasks ()
      "Skip trees that are not available for archiving"
      (save-restriction
	(widen)
	;; Consider only tasks with done todo headings as archivable candidates
	(let ((next-headline (save-excursion (or (outline-next-heading) (point-max))))
	      (subtree-end (save-excursion (org-end-of-subtree t))))
	  (if (member (org-get-todo-state) org-todo-keywords-1)
	      (if (member (org-get-todo-state) org-done-keywords)
		  (let* ((daynr (string-to-number (format-time-string "%d" (current-time))))
			 (a-month-ago (* 60 60 24 (+ daynr 1)))
			 (last-month (format-time-string "%Y-%m-" (time-subtract (current-time) (seconds-to-time a-month-ago))))
			 (this-month (format-time-string "%Y-%m-" (current-time)))
			 (subtree-is-current (save-excursion
					       (forward-line 1)
					       (and (< (point) subtree-end)
						    (re-search-forward (concat last-month "\\|" this-month) subtree-end t)))))
		    (if subtree-is-current
			subtree-end ; Has a date in this month or last month, skip it
		      nil))  ; available to archive
		(or subtree-end (point-max)))
	    next-headline))))

    ;; Setup agenda
    (setq org-agenda-dim-blocked-tasks nil)
    (setq org-agenda-compact-blocks t)

    ;; Keep done tasks from cluttering agenda
    (setq org-agenda-skip-deadline-if-done t)
    (setq org-agenda-skip-scheduled-if-done t)
    (setq org-agenda-skip-timestamp-if-done t)

    ;; Display tags far to the right
    (setq org-agenda-tags-column -140)

    ;; Make agenda views here
    ;; Have one view based on contexts
    ;; Have one view based on domains of life
    ;; Have one view based on reviewing status of projects
    ;; Consider creating a view for searching archives
    ;;     This would require changing the archive location
    (setq org-agenda-custom-commands
	  (quote (("r" "Main agenda"
		   ((agenda "" nil)
		    (tags "REFILE"
			  ((org-agenda-overriding-header "Tasks to Refile")
			   (org-tags-match-list-sublevels nil)))
		    (tags-todo "-CANCELLED/!"
			       ((org-agenda-overriding-header "Stuck Projects")
				(org-agenda-skip-function 'my/skip-non-stuck-projects)
				(org-agenda-sorting-strategy
				 '(category-keep))))
		    (tags-todo "-HOLD-CANCELLED/!"
			       ((org-agenda-overriding-header "Projects")
				(org-agenda-skip-function 'my/skip-non-projects)
				(org-tags-match-list-sublevels 'indented)
				(org-agenda-sorting-strategy
				 '(category-keep))))
		    (tags-todo "-CANCELLED/!NEXT"
			       ((org-agenda-overriding-header (concat "Project Next Tasks"
								      (if my/hide-scheduled-and-waiting-next-tasks
									  ""
									" (including WAITING and SCHEDULED tasks)")))
				(org-agenda-skip-function 'my/skip-projects-and-habits-and-single-tasks)
				(org-tags-match-list-sublevels t)
				(org-agenda-todo-ignore-scheduled my/hide-scheduled-and-waiting-next-tasks)
				(org-agenda-todo-ignore-deadlines my/hide-scheduled-and-waiting-next-tasks)
				(org-agenda-todo-ignore-with-date my/hide-scheduled-and-waiting-next-tasks)
				(org-agenda-sorting-strategy
				 '(todo-state-down effort-up category-keep))))
		    (tags-todo "-REFILE-CANCELLED-WAITING-HOLD/!"
			       ((org-agenda-overriding-header (concat "Project Subtasks"
								      (if my/hide-scheduled-and-waiting-next-tasks
									  ""
									" (including WAITING and SCHEDULED tasks)")))
				(org-agenda-skip-function 'my/skip-non-project-tasks)
				(org-agenda-todo-ignore-scheduled my/hide-scheduled-and-waiting-next-tasks)
				(org-agenda-todo-ignore-deadlines my/hide-scheduled-and-waiting-next-tasks)
				(org-agenda-todo-ignore-with-date my/hide-scheduled-and-waiting-next-tasks)
				(org-agenda-sorting-strategy
				 '(category-keep))))
		    (tags-todo "-REFILE-CANCELLED-WAITING-HOLD-Reading/!"
			       ((org-agenda-overriding-header (concat "Standalone Tasks"
								      (if my/hide-scheduled-and-waiting-next-tasks
									  ""
									" (including WAITING and SCHEDULED tasks)")))
				(org-agenda-skip-function 'my/skip-project-tasks)
				(org-agenda-todo-ignore-scheduled my/hide-scheduled-and-waiting-next-tasks)
				(org-agenda-todo-ignore-deadlines my/hide-scheduled-and-waiting-next-tasks)
				(org-agenda-todo-ignore-with-date my/hide-scheduled-and-waiting-next-tasks)
				(org-agenda-sorting-strategy
				 '(category-keep))))
		    (tags-todo "+Reading/!"
			       ((org-agenda-overriding-header (concat "Reading!"
								      (if my/hide-scheduled-and-waiting-next-tasks
									  ""
									" (including WAITING and SCHEDULED)")))
				(org-agenda-skip-function 'my/skip-project-tasks)
				(org-agenda-todo-ignore-scheduled my/hide-scheduled-and-waiting-next-tasks)
				(org-agenda-todo-ignore-deadlines my/hide-scheduled-and-waiting-next-tasks)
				(org-agenda-todo-ignore-with-date my/hide-scheduled-and-waiting-next-tasks)
				(org-agenda-sorting-strategy
				 '(category-keep))))
		    (tags-todo "-CANCELLED+WAITING|HOLD/!"
			       ((org-agenda-overriding-header (concat "Waiting and Postponed Tasks"
								      (if my/hide-scheduled-and-waiting-next-tasks
									  ""
									" (including WAITING and SCHEDULED tasks)")))
				(org-agenda-skip-function 'my/skip-non-tasks)
				(org-tags-match-list-sublevels nil)
				(org-agenda-todo-ignore-scheduled my/hide-scheduled-and-waiting-next-tasks)
				(org-agenda-todo-ignore-deadlines my/hide-scheduled-and-waiting-next-tasks)))
		    (tags "-REFILE-MAYBE/-MAYBE"
			  ((org-agenda-overriding-header "Tasks to Archive")
			   (org-agenda-skip-function 'my/skip-non-archivable-tasks)
			   (org-tags-match-list-sublevels nil)))))

		  ("c" "Context based agenda"
		   ((agenda "" nil)
		    (tags-todo "+@School-CANCELLED/!"
			       ((org-agenda-overriding-header (concat "Tasks to do at school"
								      (if my/hide-scheduled-and-waiting-next-tasks
									  ""
									" (including WAITING and SCHEDULED tasks)")))
				(org-agenda-skip-function 'my/skip-non-tasks)
				(org-tags-match-list-sublevels t)
				(org-agenda-todo-ignore-scheduled my/hide-scheduled-and-waiting-next-tasks)
				(org-agenda-todo-ignore-deadlines my/hide-scheduled-and-waiting-next-tasks)
				(org-agenda-todo-ignore-with-date my/hide-scheduled-and-waiting-next-tasks)
				(org-agenda-sorting-strategy
				 '(todo-state-down effort-up category-keep))))
		    (tags-todo "+@Office-CANCELLED/!"
			       ((org-agenda-overriding-header (concat "Tasks to do at work"
								      (if my/hide-scheduled-and-waiting-next-tasks
									  ""
									" (including WAITING and SCHEDULED tasks)")))
				(org-agenda-skip-function 'my/skip-non-tasks)
				(org-tags-match-list-sublevels t)
				(org-agenda-todo-ignore-scheduled my/hide-scheduled-and-waiting-next-tasks)
				(org-agenda-todo-ignore-deadlines my/hide-scheduled-and-waiting-next-tasks)
				(org-agenda-todo-ignore-with-date my/hide-scheduled-and-waiting-next-tasks)
				(org-agenda-sorting-strategy
				 '(todo-state-down effort-up category-keep))))
		    (tags-todo "+@Home-CANCELLED/!"
			       ((org-agenda-overriding-header (concat "Tasks to do at home"
								      (if my/hide-scheduled-and-waiting-next-tasks
									  ""
									" (including WAITING and SCHEDULED tasks)")))
				(org-agenda-skip-function 'my/skip-non-tasks)
				(org-tags-match-list-sublevels t)
				(org-agenda-todo-ignore-scheduled my/hide-scheduled-and-waiting-next-tasks)
				(org-agenda-todo-ignore-deadlines my/hide-scheduled-and-waiting-next-tasks)
				(org-agenda-todo-ignore-with-date my/hide-scheduled-and-waiting-next-tasks)
				(org-agenda-sorting-strategy
				 '(todo-state-down effort-up category-keep))))
		    (tags-todo "+@Errands-CANCELLED/!"
			       ((org-agenda-overriding-header (concat "Errands to do"
								      (if my/hide-scheduled-and-waiting-next-tasks
									  ""
									" (including WAITING and SCHEDULED tasks)")))
				(org-agenda-skip-function 'my/skip-non-tasks)
				(org-tags-match-list-sublevels t)
				(org-agenda-todo-ignore-scheduled my/hide-scheduled-and-waiting-next-tasks)
				(org-agenda-todo-ignore-deadlines my/hide-scheduled-and-waiting-next-tasks)
				(org-agenda-todo-ignore-with-date my/hide-scheduled-and-waiting-next-tasks)
				(org-agenda-sorting-strategy
				 '(todo-state-down effort-up category-keep))))
		    (tags-todo "+@Computer-CANCELLED/!"
			       ((org-agenda-overriding-header (concat "Tasks to do at the computer"
								      (if my/hide-scheduled-and-waiting-next-tasks
									  ""
									" (including WAITING and SCHEDULED tasks)")))
				(org-agenda-skip-function 'my/skip-non-tasks)
				(org-tags-match-list-sublevels t)
				(org-agenda-todo-ignore-scheduled my/hide-scheduled-and-waiting-next-tasks)
				(org-agenda-todo-ignore-deadlines my/hide-scheduled-and-waiting-next-tasks)
				(org-agenda-todo-ignore-with-date my/hide-scheduled-and-waiting-next-tasks)
				(org-agenda-sorting-strategy
				 '(todo-state-down effort-up category-keep))))
		    (tags-todo "+@Telephone-CANCELLED/!"
			       ((org-agenda-overriding-header (concat "Phone calls to make"
								      (if my/hide-scheduled-and-waiting-next-tasks
									  ""
									" (including WAITING and SCHEDULED tasks)")))
				(org-agenda-skip-function 'my/skip-non-tasks)
				(org-tags-match-list-sublevels t)
				(org-agenda-todo-ignore-scheduled my/hide-scheduled-and-waiting-next-tasks)
				(org-agenda-todo-ignore-deadlines my/hide-scheduled-and-waiting-next-tasks)
				(org-agenda-todo-ignore-with-date my/hide-scheduled-and-waiting-next-tasks)
				(org-agenda-sorting-strategy
				 '(todo-state-down effort-up category-keep))))
		    (tags-todo "+@Anywhere-CANCELLED/!"
			       ((org-agenda-overriding-header (concat "Tasks that can be done anywhere"
								      (if my/hide-scheduled-and-waiting-next-tasks
									  ""
									" (including WAITING and SCHEDULED tasks)")))
				(org-agenda-skip-function 'my/skip-non-tasks)
				(org-tags-match-list-sublevels t)
				(org-agenda-todo-ignore-scheduled my/hide-scheduled-and-waiting-next-tasks)
				(org-agenda-todo-ignore-deadlines my/hide-scheduled-and-waiting-next-tasks)
				(org-agenda-todo-ignore-with-date my/hide-scheduled-and-waiting-next-tasks)
				(org-agenda-sorting-strategy
				 '(todo-state-down effort-up category-keep))))
		    ))
		  ("d" "Domain based agenda"
		   ((agenda "" nil)
		    (tags-todo "+Personal-CANCELLED/!"
			       ((org-agenda-overriding-header (concat "Personal tasks"
								      (if my/hide-scheduled-and-waiting-next-tasks
									  ""
									" (including WAITING and SCHEDULED tasks)")))
				(org-agenda-skip-function 'my/skip-non-tasks)
				(org-tags-match-list-sublevels t)
				(org-agenda-todo-ignore-scheduled my/hide-scheduled-and-waiting-next-tasks)
				(org-agenda-todo-ignore-deadlines my/hide-scheduled-and-waiting-next-tasks)
				(org-agenda-todo-ignore-with-date my/hide-scheduled-and-waiting-next-tasks)
				(org-agenda-sorting-strategy
				 '(todo-state-down effort-up category-keep))))
		    (tags-todo "+Classwork-CANCELLED/!"
			       ((org-agenda-overriding-header (concat "College classwork ONLY"
								      (if my/hide-scheduled-and-waiting-next-tasks
									  ""
									" (including WAITING and SCHEDULED tasks)")))
				(org-agenda-skip-function 'my/skip-non-tasks)
				(org-tags-match-list-sublevels t)
				(org-agenda-todo-ignore-scheduled my/hide-scheduled-and-waiting-next-tasks)
				(org-agenda-todo-ignore-deadlines my/hide-scheduled-and-waiting-next-tasks)
				(org-agenda-todo-ignore-with-date my/hide-scheduled-and-waiting-next-tasks)
				(org-agenda-sorting-strategy
				 '(todo-state-down effort-up category-keep))))
		    (tags-todo "+College-CANCELLED/!"
			       ((org-agenda-overriding-header (concat "College tasks"
								      (if my/hide-scheduled-and-waiting-next-tasks
									  ""
									" (including WAITING and SCHEDULED tasks)")))
				(org-agenda-skip-function 'my/skip-non-tasks)
				(org-tags-match-list-sublevels t)
				(org-agenda-todo-ignore-scheduled my/hide-scheduled-and-waiting-next-tasks)
				(org-agenda-todo-ignore-deadlines my/hide-scheduled-and-waiting-next-tasks)
				(org-agenda-todo-ignore-with-date my/hide-scheduled-and-waiting-next-tasks)
				(org-agenda-sorting-strategy
				 '(todo-state-down effort-up category-keep))))
		    (tags-todo "+Work-CANCELLED/!"
			       ((org-agenda-overriding-header (concat "Work tasks"
								      (if my/hide-scheduled-and-waiting-next-tasks
									  ""
									" (including WAITING and SCHEDULED tasks)")))
				(org-agenda-skip-function 'my/skip-non-tasks)
				(org-tags-match-list-sublevels t)
				(org-agenda-todo-ignore-scheduled my/hide-scheduled-and-waiting-next-tasks)
				(org-agenda-todo-ignore-deadlines my/hide-scheduled-and-waiting-next-tasks)
				(org-agenda-todo-ignore-with-date my/hide-scheduled-and-waiting-next-tasks)
				(org-agenda-sorting-strategy
				 '(todo-state-down effort-up category-keep))))
		    ))
		  )))

    ;; Setup archiving
    (setq org-archive-location "org_archive/%s::")

    ;; Custom diary commands

    (defun org-block-schedule (y1 m1 d1 y2 m2 d2 daynames)
      (and (memq (calendar-day-of-week date) daynames) (org-block y1 m1 d1 y2 m2 d2)))
    ))
